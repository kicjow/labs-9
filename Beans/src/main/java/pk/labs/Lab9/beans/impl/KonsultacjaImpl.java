package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.Date;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

public class KonsultacjaImpl implements Consultation, Serializable {

    
    private Term term;
    
   private String student;
    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        if (term == null) {
            throw new PropertyVetoException("", null);
        }
        this.term = term;
    }
    
    public Term getTerm(){
        return this.term;
    }

    @Override
    public String getStudent() {

        return this.student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        int duration = term.getDuration();
        duration += minutes; // prolong
        this.term.setDuration(duration);
    }

    public KonsultacjaImpl(Term term, String studentName) {
        this.term = term;
        this.student = studentName;
    }

    public KonsultacjaImpl() {
        this.term = new ConsTerm(new Date(), new Date());
        this.student = "";
    }
    
   
}
