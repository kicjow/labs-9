/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;
/**
 *
 * @author st
 */


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

public class KonsultacjeLista implements ConsultationList, Serializable {

    
      List<Consultation> spotknia;
    PropertyChangeSupport prop; 
    
    @Override
    public int getSize() {
        return this.spotknia.size();
    }

    @Override
    public Consultation[] getConsultation() {
        Consultation[] tablica;
        tablica = this.spotknia.toArray(new Consultation[0]);
        return tablica;
    }
    
    public void setConsultation(Consultation[] cons){
        this.spotknia.clear();        
        this.spotknia.addAll(Arrays.asList(cons));
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.spotknia.get(index);
    }

    @Override
    public void addConsultation(Consultation consultation)
            throws PropertyVetoException {
        Consultation[] stara = this.getConsultation();
        
        if (consultation == null) {
            throw new PropertyVetoException("", null); 
        }
        
        this.spotknia.add(consultation);
        
        prop.firePropertyChange("consultation", stara, getConsultation());
        
        
       
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        prop.removePropertyChangeListener(listener);
        
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        prop.addPropertyChangeListener(listener);
    }

    public KonsultacjeLista() {
        this.prop = new PropertyChangeSupport(this);
        this.spotknia = new ArrayList<Consultation>();
    }
  
    
    
}
