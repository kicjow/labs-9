/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

/**
 *
 * @author st
 */
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author st
 */
public class Fabryka implements ConsultationListFactory {

    @Override
    public ConsultationList create() {
        return create(false);
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        ConsultationList lista = null;
        if (deserialize) {
            try {
                XMLDecoder zdekoduj = new XMLDecoder(new BufferedInputStream(new FileInputStream("plik.xml")));
                lista = (ConsultationList) zdekoduj.readObject();
                zdekoduj.close();
            } catch (Exception e) {
                lista = new KonsultacjeLista();
            }
        } else {
            lista = new KonsultacjeLista();
        }
        return lista;
    }

    @Override
    public void save(ConsultationList consultationList) {
        try {
            XMLEncoder zakoduj = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("plik.xml")));
            zakoduj.writeObject(consultationList);
            zakoduj.close();
        } catch (Exception e) {
        }
    }
    
}
