/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;
import java.util.Date;
import pk.labs.Lab9.beans.Term;
import java.io.Serializable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
/**
 *
 * @author st
 */
public class ConsTerm implements Term, Serializable {
    
    
    
    private Date beginning;
    private int duration;
    private Date end;
    
    @Override
    public Date getBegin() {
        return this.beginning;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setBegin(Date begin) {
        this.beginning = begin;
    }

    
    
    @Override
    public int getDuration() {
        return this.duration;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setDuration(int duration) {
        if (duration > 0){
       int sekundy = duration * 6000; // ilosc misekund w minucie
       this.end = new Date(this.beginning.getTime() + sekundy);
       this.duration = duration;
        }
    }

    @Override
    public Date getEnd() {
        return this.end;
    }
    
    PropertyChangeSupport prop = new PropertyChangeSupport(this);
    PropertyChangeEvent durationChnage;

    public ConsTerm(Date beginning, Date end) {
        this.beginning = beginning;
        this.end = end;
        this.duration = (int)(end.getTime() - beginning.getTime());
    }

    public ConsTerm() {
        this.beginning = new Date();
        this.end = new Date();
        this.duration = 0;
    }
    
    
    
}
