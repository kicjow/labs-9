package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = ConsTerm.class;
    public static Class<? extends Consultation> consultationBean = KonsultacjaImpl.class;
    public static Class<? extends ConsultationList> consultationListBean = KonsultacjeLista.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = Fabryka.class;
    
}
